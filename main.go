package main

import (
	"os"

	command "gitlab.com/DenisFovas/go-learning/commands"
	wc "gitlab.com/DenisFovas/go-learning/wc"
)

func setup() *command.CommandManager {
	commandManager := command.NewCommandManger()

	commands := make([]command.Command, 0)
	commands = append(commands, wc.WcCommand{})

	commandManager.Init(commands)

	return commandManager
}

func main() {
	commandManager := setup()

	args := os.Args
	if len(args) == 1 {
		commandManager.PrintMenu()

		return
	}

	output := commandManager.Run(args[1:])
	println(output)
}
