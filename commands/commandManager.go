package commands

type Command interface {
	GetName() string
	GetHelp() string
	SetFlags(flags []string)
	Run(args []string) (string, error)
}

type CommandManager struct {
	tools map[string]Command
}

func (commandManager CommandManager) Init(tools []Command) {
	for _, tool := range tools {
		commandManager.tools[tool.GetName()] = tool
	}
}

func (commandManager CommandManager) PrintMenu() {
	for _, tool := range commandManager.tools {
		print(tool.GetName() + " ")
		println(tool.GetHelp())
	}
}

func (commandManager CommandManager) Get(key string) Command {
	return commandManager.tools[key]
}

func (commandManager CommandManager) Run(args []string) string {
	if len(args) == 0 {
		// Here we need to make it different. Maybe a custom exception
		commandManager.PrintMenu()

		return ""
	}
	command := commandManager.Get(args[0])

	if len(args) == 1 {
		return command.GetHelp()
	}

	output, error := command.Run(args[1:])

	if error != nil {
		return error.Error()
	}

	return output
}

func NewCommandManger() *CommandManager {
	var cm CommandManager

	cm.tools = make(map[string]Command)

	return &cm
}
