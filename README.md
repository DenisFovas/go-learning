# go-learning

This repository will contain some small projects, for the golang.

I plan to do a set of small things and tools to better understand the
language and it's tools. Some of them will not respect the POSIX standards or the
same arguments.

## Tools

- [x] wc
- [] json parser
- [] ls  

## API

- [] Small API that returns wc result from a text
- [] Same API Endpoint but with JWT
- [] Endpoint that accepts files and runs wc on it
- [] url shorthener creation
- [] url redirect (based on the above)

### Notes

- wc is completed but will not make all of the arguments.
- Will follow.
