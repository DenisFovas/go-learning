package wc

import (
	"os"
	"strconv"
)

type InvalidNumberOfArguments struct{}

func (e *InvalidNumberOfArguments) Error() string {
	return "Invalid number of arguments"
}

type WcCommand struct {
	input string
	flags []string
}

func stringInArray(s string, arr []string) bool {
	found := false

	for _, item := range arr {
		if s == item {
			found = true
		}
	}

	return found
}

func (wc WcCommand) Run(args []string) (string, error) {

	if len(args) != 1 {
		return wc.GetHelp(), nil
	}

	filePath := args[0]
	fileData, error := os.ReadFile(filePath)

	if error != nil {
		panic(error)
	}

	wordCount := 0
	whitespaces := []string{" ", "\t", "\n", "\r\n"}

	for index, char := range fileData {
		if index == 0 && !stringInArray(string(char), whitespaces) {
			continue
		}

		if stringInArray(string(char), whitespaces[:]) && !stringInArray(string(fileData[index-1]), whitespaces[:]) {
			wordCount++
		}
	}

	value := strconv.Itoa(wordCount)

	return value, nil
}

func (wc WcCommand) GetName() string {
	return "wc"
}

func (wc WcCommand) SetFlags(flags []string) {
	wc.flags = flags
}

func (wc WcCommand) GetHelp() string {
	return "wc [filepath_relative_to_exec]"
}
